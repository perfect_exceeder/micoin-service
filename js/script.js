/**
 * Created by h8every1 on 03.12.2014.
 */
( function( $ ) {

	$('#modal-window').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var remote_html = button.data('url') // Extract info from data-* attributes

		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var modal = $(this)


		if (remote_html) {
			modal.find('.modal-content').load(remote_html);
		} else {
			return false;
		}

	})

})(jQuery);